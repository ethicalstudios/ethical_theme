<?php
/**
 * @file
 * ethical_theme.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ethical_theme_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'eu_cookie_compliance_en';
  $strongarm->value = array(
    'popup_enabled' => 1,
    'popup_clicking_confirmation' => 1,
    'popup_position' => 0,
    'popup_agree_button_message' => 'Continue',
    'popup_disagree_button_message' => 'Find out more',
    'popup_info' => array(
      'value' => '<h2>Cookies on this website</h2><p>We use cookies to ensure that we give you the best experience on our website. If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on this website. However, if you would like to, you can <a href="http://www.aboutcookies.org/">change your cookie settings</a> at any time.</p>',
      'format' => 'panopoly_wysiwyg_text',
    ),
    'popup_agreed_enabled' => 0,
    'popup_hide_agreed' => 0,
    'popup_find_more_button_message' => 'More info',
    'popup_hide_button_message' => 'Hide',
    'popup_agreed' => array(
      'value' => '<h2>Thank you for accepting cookies</h2><p>You can now hide this message or find out more about cookies.</p>',
      'format' => 'panopoly_wysiwyg_text',
    ),
    'popup_link' => 'https://ico.org.uk/for_organisations/privacy_and_electronic_communications/the_guide/cookies',
    'popup_link_new_window' => 1,
    'popup_height' => '',
    'popup_width' => '100%',
    'popup_delay' => '1',
    'popup_bg_hex' => '505050',
    'popup_text_hex' => 'ffffff',
    'domains_option' => '1',
    'domains_list' => '',
    'exclude_paths' => '',
  );
  $export['eu_cookie_compliance_en'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'fences_default_classes';
  $strongarm->value = 1;
  $export['fences_default_classes'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'fences_default_markup';
  $strongarm->value = 1;
  $export['fences_default_markup'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_frisbee_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 0,
    'toggle_slogan' => 0,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'zentropy_feed_icons' => 0,
    'zentropy_shiv' => 1,
    'zentropy_shiv_google' => 0,
    'zentropy_prompt_cf' => 'IE 7',
    'zentropy_ie_edge' => 1,
    'zentropy_responsive_enable' => 1,
    'zentropy_responsive_fallback' => '768',
    'zentropy_polyfill_mediaquery' => 1,
    'zentropy_polyfill_mediaquery_type' => '0',
    'zentropy_polyfill_selectivizr' => 1,
    'zentropy_polyfill_pie' => TRUE,
    'zentropy_polyfill_boxsizing' => TRUE,
    'zentropy_polyfill_scalefix' => 0,
    'zentropy_polyfill_ie7' => 0,
    'zentropy_polyfill_ie8' => 0,
    'zentropy_polyfill_ie9' => 0,
    'zentropy_polyfill_ie_google' => 0,
    'zentropy_polyfill_ie_png' => 0,
    'zentropy_mobile_viewport' => 'width=device-width, target-densitydpi=160dpi, initial-scale=1.0',
    'zentropy_mobile_touch' => 1,
    'zentropy_mobile_touch_57' => '',
    'zentropy_mobile_touch_72' => '',
    'zentropy_mobile_touch_114' => '',
    'zentropy_form_css' => 0,
    'zentropy_form_tooltip' => 0,
    'zentropy_coolinput' => 1,
    'zentropy_coolinput_labels' => 0,
    'zentropy_zen_tabs' => 0,
    'zentropy_tabs_float' => 0,
    'zentropy_tabs_node' => 1,
    'zentropy_breadcrumb_show' => 1,
    'zentropy_breadcrumb_hideonlyfront' => 1,
    'zentropy_breadcrumb_showtitle' => 0,
    'zentropy_breadcrumb_separator' => ' &#187; ',
    'zentropy_clear_registry' => 0,
    'zentropy_wireframe_mode' => 0,
    'zentropy_cachebuster_css' => '0',
    'zentropy_ajax_loader' => 0,
    'zentropy_scripts_footer' => 0,
    'zentropy_menu_span' => 0,
    'zentropy_settings__active_tab' => 'edit-zentropy-general',
  );
  $export['theme_frisbee_settings'] = $strongarm;

  return $export;
}
