<?php
// Plugin definition
// Used for node edit form
// This is the version of burr flipped that is from panopoly and not radix layouts.
// Because I overwrote the panopoly layouts with radix layouts it was necessary to bring the panopoly version back from the dead here
$plugin = array(
  'title' => t('Open Ethical Admin'),
  'icon' => 'oe-admin.png',
  'category' => t('Open Ethical'),
  'theme' => 'oe_admin',
  'css' => 'oe_admin.css',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
