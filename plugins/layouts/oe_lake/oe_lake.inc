<?php
// Plugin definition
$plugin = array(
  'title' => t('OE Lake'),
  'icon' => 'oe-lake.png',
  'category' => t('Open Ethical'),
  'theme' => 'oe_lake',
  'regions' => array(
    'header' => t('Header'),
    'column1' => t('Row 1: First Column'),
    'column2' => t('Row 1: Second Column'),
    'column3' => t('Row 1: Third Column'),
    'full' => t('Full'),
    'secondarycolumn1' => t('Row 2: First Column'),
    'secondarycolumn2' => t('Row 2: Second Column'),
    'secondarycolumn3' => t('Row 2: Third Column'),
    'footer' => t('Footer'),
  ),
);
