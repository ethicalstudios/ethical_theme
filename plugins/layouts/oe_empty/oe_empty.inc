<?php
// Plugin definition
$plugin = array(
  'title' => t('Open Ethical Empty'),
  'icon' => 'oe-empty.png',
  'category' => t('Open Ethical'),
  'theme' => 'oe_empty',
  'css' => 'oe-empty.css',
  'regions' => array(
    'contentmain' => t('Content'),
  ),
);
