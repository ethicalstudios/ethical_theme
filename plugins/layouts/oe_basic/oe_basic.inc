<?php
// Plugin definition
// Used widely across the site for teasers and featured items.
// Has some elaborate CSS to make it look good across lots of screen sizes.
$plugin = array(
  'title' => t('Open Ethical Basic'),
  'icon' => 'oe-basic.png',
  'category' => t('Open Ethical'),
  'theme' => 'oe_basic',
  'css' => 'oe-basic.css',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
